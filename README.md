## Tobi Chammings Projects

### UTrack
University Android App to allow students to track phone usage during lectures, however also extends to any Calendar on the user's phone
https://bitbucket.org/tobiCham/utrack/src

### Arduino Versus Game
A versus block shooter game, implemented on Arduino Uno. Each player views the same blocks falling, and must dodge or shoot them
https://bitbucket.org/tobiCham/arduino-versus-game/src

### Squared
Java 2D tile based game where you have to traverse the level to reach the exit, avoiding obstacles which can send you back to the start of the level
https://bitbucket.org/tobiCham/squared/src

### Secret Images
Hides an encrypted message inside a PNG image
https://bitbucket.org/tobiCham/secret-images/src